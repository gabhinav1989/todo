import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';
import {IUser} from './IUser';
import {UserService} from './user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

@Injectable({
  providedIn: 'root'
})

export class UsersComponent implements OnInit {
  constructor(private userService: UserService) {

  }

  // declare all variables
  tableTitle = 'User List';
  users: IUser[];
  name: string;
  email: string;

  ngOnInit() {
    this.userService.getUsers().subscribe(result => {
      this.users = result;
    });
  }
}
