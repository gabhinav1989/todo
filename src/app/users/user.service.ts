import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {IUser} from './IUser';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  getUsers(): Observable<IUser[]> {
    return this.httpClient.get<IUser[]>(environment.apiUrl + '/users');
  }
}
